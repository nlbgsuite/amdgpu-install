#####################################
Using the ``amdgpu-install`` Script
#####################################

.. contents:: Table of Contents

----------

Script Types
============

The AMDGPU graphics stack offers the following scripts to help you install
a coherent set of stack components:

- ``amdgpu-install``
- ``amdgpu-pro-install``

Each of these scripts support the same set of arguments that are
explained in the following sections of this page.

.. note::

	 - Although examples on this page show ``amdgpu-install`` script being
	   invoked, you can use same arguments with ``amdgpu-pro-install`` script.
	 - Running the script ``amdgpu-pro-install`` is synonymous to running the
	   script as ``amdgpu-install --pro``.

------------

Purpose
=======

The ``amdgpu-install`` script does the following:

- Simplifies the installation of the AMDGPU graphics and compute stack by
  encapsulating the distribution specific package installation logic and by
  using command line options that allow to specify the:

   * Variant of the AMDGPU stack to be installed (All-Open or Pro)
   * Combination of components (in case of the Pro stack)

- Performs post-install checks to verify whether the installation was performed
  successfully.
- Installs the uninstallation script to allow you to remove the whole
  AMDGPU stack from the system by using a single command.

The script is packaged into the AMDGPU-Pro tar archive.

---------

Invoking the ``amdgpu-install`` Script
=======================================

Once you have downloaded and extracted the AMDGPU-Pro tar archive, access the
directory where you extracted it. Invoke the script as ``./amdgpu-install``.

.. note:: Use of the ``-y`` option is recommended to avoid prompts from the
      package manager.

-----------------

Scenarios
=========

Specifying an OpenCL Implementation
-----------------------------------

There are two OpenCL implementations available:

- **ROCr**: Provides support for *Vega 10* and newer hardware.
- **Legacy**: Provides support for hardware older than *Vega 10*.

You can either install one or both implementations.

.. note:: In previous releases, the "PAL" implementation was used for Vega 10
      and newer, but this has been removed in favour for the ROCr backend,
      which is derived from the ROCm open source stack

.. note:: To use the ROCr implementation of OpenCL, the running user might
      need additional permissions. Usually the user must be added to the
      "render" group or to the "video" group. See the notes in
      :ref:`Installation-OpenCL-Optional-Component` for more details.

OpenCL is installed using the ``--opencl`` option, as follows:

+------------------------+---------------------------------------------+
|**Option**              | **Description**                             |
+------------------------+---------------------------------------------+
|``--opencl=rocr``       | Installs ROCr OpenCL.                       |
+------------------------+---------------------------------------------+
|``--opencl=legacy``     | Installs legacy OpenCL.                     |
+------------------------+---------------------------------------------+
|``--opencl=rocr,legacy``| Installs both ROCr and legacy OpenCL.       |
+------------------------+---------------------------------------------+

Applicable examples on this page demonstrate installation of both OpenCL
implementations.

Viewing Help or Usage Output
-----------------------------
Executing the script with ``-h`` or ``--help`` argument provides the help or
usage output.

.. code-block:: bash

  $ ./amdgpu-install -h


Installing Non-Interactively
-----------------------------

Using the argument ``-y`` informs the package management system to assume
the answer *yes* for any interactive question. This option allows the
install script to embed itself inside other automated scripts.

Example:

.. code-block:: bash

  $ ./amdgpu-install -y


Adding Advanced Package Management Functionality
---------------------------------------------------

More advanced functionality can be obtained by providing the installation
script with arguments to pass through to the package management software of the
distribution you are using.

- On Ubuntu, refer to the `apt man page <https://linux.die.net/man/8/apt>`_ for more information.
- On Red Hat, refer to `yum man page <https://linux.die.net/man/8/yum>`_ for more information.
